﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LlNQBasic_part1
{
    class Program
    {

        /// <summary>
        /// LINQ (Language Integrated Query) is uniform query syntax in C# and VB.NET to retrieve data from different sources and formats
        /// 
        /// https://www.tutorialsteacher.com/linq/linq-filtering-operators-where
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Student[] studentArray = {
            new Student() { StudentID = 1, StudentName = "John", Age = 18 },
            new Student() { StudentID = 2, StudentName = "Steve",  Age = 21 },
            new Student() { StudentID = 3, StudentName = "Bill",  Age = 25 },
            new Student() { StudentID = 4, StudentName = "Ram" , Age = 20 },
            new Student() { StudentID = 5, StudentName = "Ron" , Age = 31 },
            new Student() { StudentID = 6, StudentName = "Chris",  Age = 17 },
            new Student() { StudentID = 7, StudentName = "Rob",Age = 19  },
        };

        IList<Student> studentList = new List<Student>() {
        new Student() { StudentID = 1, StudentName = "John", Age = 18 } ,
        new Student() { StudentID = 2, StudentName = "Steve",  Age = 16 } ,
         new Student() { StudentID = 2, StudentName = "Clark",  Age = 15 } ,
         new Student() { StudentID = 2, StudentName = "Clark",  Age = 17 } ,
        new Student() { StudentID = 3, StudentName = "Bill",  Age = 25 } ,
         new Student() { StudentID = 3, StudentName = "Bill",  Age = 22 } ,
        new Student() { StudentID = 4, StudentName = "Ram" , Age = 20 } ,
        new Student() { StudentID = 5, StudentName = "Ron" , Age = 19 }
    };

            //Excercises

            //######   WHERE  section 1     #####################################
            //Find all Student objects from an array of Students where the age is between 12 and 20 (for teenage 13 to 19):
            Student[] studentsAge = studentArray.Where(s => s.Age > 12 && s.Age < 20).ToArray();
           // ShowArray(studentsAge);
            //Find first student whose name is Bill 
            Student Bill = studentArray.Where(n => n.StudentName == "Bill").FirstOrDefault();
            //  Console.WriteLine("The Bill is a studen with Id is: " + Bill.StudentID);
            // find student whose StudentID is 5
            Student IdIs5 = studentArray.Where(id => id.StudentID == 5).FirstOrDefault();
           // Console.WriteLine("The name student with id = 5 is: " + IdIs5.StudentName);

        // ##### Where with special Index overload!!!! #######
            // filter out odd elements in the collection and return only even elements 
            //i- index of value(not id or age)(counting from 0)
            //secend overload inncludes index of current lement of collection
            var filterEvenByIndex = studentList.Where((sa, i) => {
                if(i % 2 == 0) {
                    return true;
                    }
                return false;
            });

            foreach (var item in filterEvenByIndex)
            {
              //  Console.WriteLine(item.StudentName);
            }



            //#############################   OFType section 2  ########################

            //def: The OfType operator filters the collection based on the ability to cast an element in a collection to a specified type.

            //Mixed collection
            //Ilist -> is get possibilities to create non-generic collection
            //ArrayList ->implemet Ilist

            IList mixedList = new ArrayList();
            
            mixedList.Add(0);
            mixedList.Add("One");
            mixedList.Add("Two");
            mixedList.Add(3);
            mixedList.Add(new Student() { StudentID = 1, StudentName = "Bill" });

            //select string type
            var stringResult = from str in mixedList.OfType<string>() select str;

            //Linq method Syntax -> Not implement for non-generic collection !!!!!!!
            //string stringResult2 = mixedList.Where

            //select int type
            var intResult = from inti in mixedList.OfType<int>() select inti;

            //selectStudent type
            var studentType = from stud in mixedList.OfType<Student>() select stud.StudentName;

            foreach (var strin in stringResult)
            {
              //  Console.WriteLine(strin);
            }

            //#############################   OrderBY section 3  ########################

            //OrderBY, OrderBYDescending

            //....OrderBY
            var orderByResultName = from student in studentList
                                    orderby student.StudentName
                                    select student;

            var orderBYAge = studentArray.OrderBy(s => s.Age);

            //... OrderBYDescending
            var OrderBYDescendingName = from student in studentList
                                        orderby student.StudentName descending
                                        select student;

            var orderbyDescendingAge = studentArray.OrderByDescending(s => s.Age);

            foreach (var item in orderbyDescendingAge)
            {
              //  Console.WriteLine(item.StudentName);
            }

            // Multiple Sorting

            var OrderBYResult = from s in studentList
                                orderby s.StudentName, s.Age
                                select new { s.StudentName, s.Age };

            var orderByResult2 = from s in studentList
                                 orderby s.Age, s.StudentName
                                 select new { s.StudentID, s.StudentName , s.Age };

            foreach (var item in orderByResult2)
            {
              //  Console.WriteLine(item);

            }

            //#############################   ThenBY section 4  ########################

            //def: ThenBy and ThenByDescending extension methods are used for sorting on multiple fields.

            //Linq in first sort collection by based primary field, then use sorting by ThenBY parameterer

            var thenByResult = studentList.OrderBy(s => s.StudentName).ThenBy(s => s.Age);

            //descending
            var thenBydescending = studentList.OrderBy(s => s.StudentName).ThenByDescending(s => s.Age);


            //#############################   GroupBy & ToLookup section 5  ########################

            //Summary
            //1.GroupBy & ToLookup return a collection that has a key and an inner collection based on a key field value.
            //2.The execution of GroupBy is deferred whereas that of ToLookup is immediate.
            //3.A LINQ query syntax can be end with the GroupBy or Select clause.

            //gRUUPING BY speify kEY !!!!!. iN THIS CASE the key is by value that we group by 

            //Age key
            var groupedResult = from s in studentList
                                group s by s.Age;

            //foreach (var ageGroup in groupedResult)
            //{
            //    Console.WriteLine("Age group: {0} " , ageGroup.Key);  //each group has key

            //    foreach (var item in ageGroup)
            //    {
            //        Console.WriteLine("Student Name: {0}" , item.StudentName);
            //    }
            //}

            //Name key
            var groupByName = from s in studentList
                              group s by s.StudentName;


            //foreach (var nameGroup in groupByName )
            //{
            //    Console.WriteLine("Name group: {0}", nameGroup.Key);

            //    foreach (var name in nameGroup)
            //    {
            //        Console.WriteLine("Student Name: {0} and Age: {1} ", name.StudentName , name.Age);
            //    }
            //}

            //MEthod Syntax

            //var groupedbyAge = studentList.GroupBy(s => s.Age);

            //foreach (var ageGroup in groupedbyAge)
            //{
            //    Console.WriteLine("Age group: {0}" , ageGroup.Key);

            //    foreach (var name in ageGroup)
            //    {
            //        Console.WriteLine("Student Name: {0} ", name.StudentName);
            //    }
            //}


            //  ToLookUp
            var lookupReslut = studentList.ToLookup(s => s.Age);

            var lookupReslutOrdered = studentList.ToLookup(s => s.Age).OrderBy(s => s.Key); //its okey too

            //foreach (var group in lookupReslut)
            //{
            //    Console.WriteLine("Age Group: {0}", group.Key);
            //    foreach (var name in group)
            //    {
            //        Console.WriteLine("The name is: {0}" , name.StudentName);
            //    }
            //}

            //############################### JOIN section 5 ########################################

            //Def: The joining operators joins two sequences(collections)

            //Join - joins two collections based on key and returns a resulted sequence
            //GroupJoin - joins two sequences based keys and returns groups of sequences. It is like Left Outer Join of Sql

            IList<Student1> studentList2 = new List<Student1>() {
                 new Student1() { StudentID = 1, StudentName = "John", StandardID =1 },
                 new Student1() { StudentID = 2, StudentName = "Moin", StandardID =1 },
                 new Student1() { StudentID = 3, StudentName = "Bill", StandardID =2 },
                 new Student1() { StudentID = 4, StudentName = "Ram" , StandardID =2 },
                  new Student1() { StudentID = 4, StudentName = "Rama" , StandardID =3 },
                 new Student1() { StudentID = 5, StudentName = "Ron"  }
                    };


            IList<Standard> standardList = new List<Standard>() {
                  new Standard(){ StandardID = 1, StandardName="Standard 1"},
                  new Standard(){ StandardID = 2, StandardName="Standard 2"},
                  new Standard(){ StandardID = 3, StandardName="Standard 3"}
                };

            IList<string> strList1 = new List<string>() {
                    "One",
                    "Two",
                    "Three",
                    "Four",
                    "Six"
                  };

            IList<string> strList2 = new List<string>() {
                    "One",
                    "Two",
                    "Three",
                    "Five",
                    "Six"
             };

            //example 1) joins two collections and return new collection that includes matching strings is both the collection

            var innerJonin = strList1.Join(strList2,
                str1 => str1,
                str2 => str2,
                (str1, str2) => str1);


            //LINQ Method Join
            var innerJoin = studentList2.Join(  //outer sequence(collection)
                standardList,  //inner sequence(collection)
                student => student.StandardID, //outerKey(first matching field)
                standard => standard.StandardID, // innerkeySelector (secend matching field)
                (student, standard) => new  //result selector(new collection)
                {
                    StudentName = student.StudentName,
                    StandardName = standard.StandardName
                });


            //Linq Query implementation
            var innerJoinuery = from s in studentList2 //outer collection
                                join st in standardList //inner collection
                                on s.StandardID equals st.StandardID  //key selector
                                select new
                                {
                                    StudentName = s.StudentName,
                                    StandardName = st.StandardName,
                                };

            //foreach (var item in innerJoin)
            //{
            //    Console.WriteLine(item);
            //}

            //######################################### GroupJoin section 6  #######################################


            //Linq method syntax

            var groupJon = standardList.GroupJoin(studentList2, //inner sequence
                            standard => standard.StandardID,  //outKeySelectoe
                            student => student.StandardID,  //innerKeySelector
                            (standard, studentsGroup) => new   //resultSelector
                            {
                                Students = studentsGroup,
                                StandardFullName = standard.StandardName
                            });

            //Query syntax
            
            var groupJoinQuerySyntax = from std in standardList
                                       join s in studentList2
                                       on std.StandardID equals s.StandardID
                                       into studentGruop
                                       select new
                                       {
                                           Students = studentGruop,
                                           StandardName = std.StandardName
                                       };

            //foreach (var item in groupJon)
            //{
            //    Console.WriteLine(item.StandardFullName);

            //    foreach (var s in item.Students)
            //    {
            //        Console.WriteLine(s.StudentName);
            //    }               


            //}

            //############################################ Select section 7 ####################################################

            //Def: Select operator always returns an IEnumerable collection which contains elements based on a transformation function

            //Query syntax
            var selectReusult = from s in studentList
                                select s.StudentName; //return a string collection of StudentName


            // returns collection of anonymous objects with Name and Age property
            var selectResult = from s in studentList
                               select new
                               {
                                   Name = "Mr. " + s.StudentName,
                                   Age = s.Age
                               };

            //MethodSynthax

            //return a annymouus collection with Name and Age
            var selectSynthaxResult = studentList.Select(s => new { Name = s.StudentName, Age = s.Age });



            //foreach (var item in selectResult)
            //{
            //    Console.WriteLine("Student Name: {0} ,Age: {1}", item.Name, item.Age);
            //}

            //############################################ All,Any , Contains section 8 ####################################################

            //Quantifier operators - evaluate elements of the sequence on some condition and return a boolen value to indicate that the some or alll emlements satisfy the condition 
            //All - Checks if all the elements in a sequence satisfies the specified condition 
            //Any - Checks if any of the elements in a sequence satisfies the specified condition 
            //Contains - Checks if the sequence contains a specific element 

            bool areAllStudentsTeenAger = studentList.All(s => s.Age > 12 && s.Age < 20); //return false(boolen type)

            bool isAnyStudentTeenAgger = studentList.Any(s => s.Age > 12 && s.Age < 20);

            IList<int> intList = new List<int>() { 1, 2, 3, 4, 5 };

            //primitive example Containb
            bool resultContain = intList.Contains(3); //the same type like a collection

            //More complex Contain
            Student std1 = new Student() { StudentID = 3, StudentName = "Bill" };

            //check student contain Bill
            bool resultCntain2 = studentList.Contains(std1);
           // Console.WriteLine(resultCntain2); //return false is is becouse Contains extension method only compares reference of an object nut not the actual values of object.
                                                //so if we wanna compare a value we should implementing IequalityComparer interface - its return boolean

            //secend overloads
            Student studentToCompare = new Student { StudentID = 3, StudentName = "Bill" };
            bool rsultWithComparer = studentList.Contains(studentToCompare, new StudentCompaer()); //return true


            //Console.WriteLine(rsultWithComparer); 


            //######################################## Agregate section 9  ########################################################################

            //Def: The aggregation operators perform mathematical operations like Average, Aggregate, Count, Max, Min and Sum, on the numeric property of the elements in the collection
            //Aggragate method performs an accumulate operation

            //example comma sepatated elements - one parameter
            IList<String> strList = new List<String>() { "One", "Two", "Three", "Four", "Five" };

            //accumuate 
            var commaSeparatedString = strList.Aggregate((s1, s2) => s1 + " , " + s2);

            //Accumulate method with seed value. (Secend Overload) using string like a seed value - two parameters
            string commaSeparatedStudentNames = studentList.Aggregate<Student, string>(
                                                "Student Names: ", //seed value
                                                (str, s) => str += s.StudentName + ",");

            //
            int SumOfStudeentAge = studentList.Aggregate<Student, int>(
                0,  //starting value for Age
                (totalAge, student) => totalAge += student.Age);  //TotalAge == 0(starting value) + sum all Ages in loop

            //Agregation with Resut Selector(third overload) - three parameters

            string commaSeparatedStudentsName3 = studentList.Aggregate<Student, string, string>(
                String.Empty,  //seed value
                (str, student) => str += student.StudentName + " , ",  //returns result using seed value, String.Empty  goes to lambda expression as a strong 
                str => str.Substring(0, str.Length - 1));  //result selector that removes last comma

            //######################################## Agregate Average section 10  ########################################################################

            //def: Average extension method calculates the average of the numeric items in the collection. Average method returns nullable or non-nullable decimal, double or float value.

            IList<int> intList2 = new List<int> () { 10, 20, 30 };


            var average = intList2.Average();

            double averageAgeOdStudent = studentList.Average(student => student.Age);  //not returns integer


            //######################################## Element Operators: ElementAt, ElementAtOrDefault section 11  ########################################################################

            //The ElementAt() - returns an element from friom the specified index from a given collection.  If the specified index is out of the range of a collection then it will throw an Index out of range exception ERROR!!!. 

            //The ElementAtOrDefault() - also returns  method also returns an element from the specified index from a collaction and if the specified index is 
            //out of range of a collection then it will return a default VALUE!!!! of the data type instead of throwing an error.

            IList<int> intList3 = new List<int>() { 10, 21, 30, 45, 50, 87 };
            IList<string> strList3 = new List<string>() { "One", "Two", null, "Four", "Five" };

            //Console.WriteLine("Element in intList: {0}", intList3.ElementAt(0));  //out of range Error
            //Console.WriteLine("Element in intList: {0}", strList3.ElementAt(0));

            //Console.WriteLine("3rd Element in intList: {0}", intList3.ElementAtOrDefault(9));  //default value in integer == 0
            //Console.WriteLine("3rd Element in strList: {0}", strList3.ElementAtOrDefault(9));  //drfault value in string == null

            //######################################## Element Operators: First, FIrstOrDefault section 12  ########################################################################

            //First	- Returns the first element of a collection, or the first element that satisfies a condition.
            //FirstOrDefault - Returns the first element of a collection, or the first element that satisfies a condition. Returns a default value if index is out of range.

            List<int> intList4 = new List<int>() { 7, 10, 21, 30, 45, 50, 87 };
            IList<string> strList4 = new List<string>() { null, "Two", "Three", "Four", "Five" };
            IList<string> emptyList4 = new List<string>();

            //#######################   First ##################

            //  Console.WriteLine("Firt in collectiom(first overload) {0}" , intList4.First());

            //   Console.WriteLine("Firt in collectiom(first overload) {0}", strList4.First());

            // Console.WriteLine("Firt in collectiom(first overload) {0}", emptyList4.First());  //Throw Exception !!!

            //secend overload satisfies a condition
            //  Console.WriteLine("1st Element in intList: {0} is Even number", intList4.First(x=> x % 2 == 0)); 

            //######################## FirstorDefault

            //  Console.WriteLine("1st Element in intList: {0}", intList4.FirstOrDefault());

            //satisfed condition
            //  Console.WriteLine("1st Element in intList: {0}", intList4.FirstOrDefault(x=> x > 45));

            //empty or out of range
            //  Console.WriteLine("1st Element in intList: {0} Out of Range", intList4.FirstOrDefault(x => x > 1000)); //it gest default value for integer == 0


            //######################################## Element Operators: Last, LastOrDefault section 13  ########################################################################

            //Last -> Returns the last element from a collection, or the last element that satisfies a condition. Out of range == Error
            //LastOrDefault -> Returns the last element from a collection, or the last element that satisfies a condition. Returns a default value if no such element exists.

            //Console.WriteLine("Last Element in intList: {0}", intList4.Last());

            //Console.WriteLine("Last Element in intList: {0}", intList4.Last(x => x < 80));

            //Console.WriteLine("Last Element in intList: {0}", intList4.LastOrDefault(x => x < 80));

            //Console.WriteLine("Last Element in intList: {0}", intList4.LastOrDefault());

            //Console.WriteLine("Last Element in intList: {0}", emptyList4.LastOrDefault());  //default value for string is Null!!!!

            //######################################## Element Operators: Single, SingleOrDefault section 14  ########################################################################

            //Single -> Returns the only element from a collection, or the only element that satisfies a condition.If Single() found no elements or more than one elements in the collection then throws InvalidOperationException.

            //SingleOrDefault -> The same as Single, except that it returns a default value of a specified generic type, instead of throwing an exception if no element found for the specified condition.
            //                  However, it will thrown InvalidOperationException if it found more than one element for the specified condition in the collection.

            IList<int> oneElementList = new List<int>() { 7 };
            IList<int> intList5 = new List<int>() { 7, 10, 21, 30, 45, 50, 87 };
            IList<string> strList5 = new List<string>() { null, "Two", "Three", "Four", "Five" };
            IList<string> emptyList = new List<string>();

            //  Console.WriteLine("The only element in oneElementList: {0}", oneElementList.Single());
            //  Console.WriteLine("The only element in oneElementList: {0}",oneElementList.SingleOrDefault());

            // Console.WriteLine("The only element in oneElementList: {0}", intList5.Single(x=> x >21)); //more then one
            //  Console.WriteLine("The only element in oneElementList: {0}", intList5.SingleOrDefault(x => x > 21)); //more then one

            // Console.WriteLine("Element in emptyList: {0}", emptyList.Single());
            // Console.WriteLine("Element in emptyList: {0}", emptyList.SingleOrDefault()); //for empty default null value

            //    Console.WriteLine("The only element which is less than 10 in intList: {0}",intList5.Single(i => i < 6));
            // Console.WriteLine("The only element which is less than 10 in intList: {0}", intList5.SingleOrDefault(i => i < 50));  //If no one is error or more than on error

            //SingleORDefault Why(:
            //If ine result -> return result
            //else if no result -> return default value
            //else if resilt > 1 -> error more than one

            //######################################## Element Operators: SequenceEqual section 15  ########################################################################

            //The sequenceEqual -> checks wheather the number of elements, value of each element and order of elements in two collections are equal or not

            IList<string> strList10 = new List<string>() { "One", "Two", "Three", "Four", "Three" };

            IList<string> strList20 = new List<string>() { "One", "Two", "Three", "Four", "Three" };

         //   bool isEqual = strList10.SequenceEqual(strList20); //return true in Order list



            IList<string> strList10noOrder = new List<string>() { "One", "Three", "Two", "Four", "Three" };

            IList<string> strList20noOrder = new List<string>() { "One", "Two", "Three", "Four", "Three" };

            // bool isEqual = strList10noOrder.SequenceEqual(strList20noOrder);  //In unOrder list return false


            //####### The SequenceEqual extension method checks the references of two objects to determine whether two sequences are equal or not. ///


            Student studentOne = new Student() { StudentID = 1, StudentName = "Bill" };

            //First List(reference to object studentOne)
            IList<Student> studentsList1 = new List<Student>() { studentOne};

            //Secand list(reference to object studentOne)
            IList<Student> studentsList2 = new List<Student>() { studentOne };

            bool isEqual = studentsList1.SequenceEqual(studentsList2);

            Student std11 = new Student() { StudentID = 1, StudentName = "Bill" };
            Student std22 = new Student() { StudentID = 1, StudentName = "Bill" };

            //reference to two separated objects(two students)
            IList<Student> studentList3 = new List<Student>() { std11 };

            IList<Student> studentList4 = new List<Student>() { std22 };


            bool isQgual2 = studentList3.SequenceEqual(studentList4); //return false  becouse we compare two another objects

            //#########  If we wanna compare value of objects we must use Comparer!!!!!

            //Here!!!!
            bool isEqualBycomparer = studentList3.SequenceEqual(studentList4, new StudentComparar2());

            Console.WriteLine(isEqualBycomparer);  // return true




            Console.ReadLine();

        }
        public static void ShowArray(Student[] ArrayToShow)
        {
            foreach (var item in ArrayToShow)
            {
                Console.WriteLine(item.StudentName);
            }
            Console.ReadLine();

        }
    }


    class Student
    {
        public int StudentID { get; set; }
        public String StudentName { get; set; }
        public int Age { get; set; }
    }

    public class Standard
    {
        public int StandardID { get; set; }
        public string StandardName { get; set; }
    }

    public class Student1
    {
        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public int StandardID { get; set; }
    }

    //Implementation od IEqualityComparer 
    class StudentCompaer : IEqualityComparer<Student>
    {
        public bool Equals(Student x, Student y)
        {
            if (x.StudentID == y.StudentID &&
                x.StudentName.ToLower() == y.StudentName.ToLower())
                return true;

            //else ... return false

            return false;
        }

        public int GetHashCode(Student obj)
        {
            return obj.GetHashCode();
        }

    }

    //the same comparar like  StudentCompaer!!
    class StudentComparar2 : IEqualityComparer<Student>
    {
        public bool Equals(Student x, Student y)
        {
            if (x.StudentID == y.StudentID && x.StudentName.ToLower() == y.StudentName.ToLower())
                return true;

            return false;
        }

        public int GetHashCode(Student obj)
        {
            return obj.GetHashCode();
        }
    }
}

